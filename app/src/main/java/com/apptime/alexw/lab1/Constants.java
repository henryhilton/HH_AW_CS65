package com.apptime.alexw.lab1;

import android.location.Criteria;

/**
 * Created by Alex W on 14/11/2017.
 * Adapted from http://www.truiton.com/2014/10/android-foreground-service-example/
 *
 */

public class Constants {

    protected static Criteria getCriteria() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(true);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(true);
        criteria.setCostAllowed(true);
        return criteria;
    }

    public interface ACTION {

        static final int SERVICE_PUSH_NOTIFICATION_CHANNEL = 22;
        String STOP_ACTION = "com.apptime.alexw.lab1.action.stop";
        String MAIN_ACTION = "com.apptime.alexw.lab1.action.main";
        String STARTFOREGROUND_ACTION = "com.apptime.alexw.lab1.action.startforeground";
        String STOPFOREGROUND_ACTION = "com.apptime.alexw.lab1.action.stopforeground";
        static final int MAP_REQUEST_CODE = 1;
    }

    public interface NOTIFICATION_ID {
        int FOREGROUND_SERVICE = 101;
    }

}
