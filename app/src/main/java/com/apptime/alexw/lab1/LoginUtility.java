package com.apptime.alexw.lab1;

/**
 * Created by henryhilton on 10/10/17.
 */

// holds the base url constant and contains a getter method for an instance of Login Service
public class LoginUtility {

    private LoginUtility() {}

        public static final String BASE_URL = "http://cs65.cs.dartmouth.edu";

        public static LoginService getLoginService() {

            return LoginClient.getClient(BASE_URL).create(LoginService.class);
        }
}
