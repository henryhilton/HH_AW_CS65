package com.apptime.alexw.lab1;

/**
 * Created by henryhilton on 10/9/17.
 */

import java.util.List;

import retrofit2.http.*;
import retrofit2.*;

// interface to direct each type of API call and connect with models
public interface LoginService {

    @GET("/nametest.pl")
    Call<GetNameAvailabilityModel> getNameAvailability(@Query("name") String name);

    @POST("/profile.pl")
    Call<PostSaveProfileModel> postSaveProfile(@Body UserProfileData uploadData);

    @GET("/profile.pl")
    Call<GetProfileModel> getProfile(@Query("name") String name, @Query("password") String password);

    @GET("/catlist.pl")
    Call<List<CatModel>> getCatList(@Query("name") String name, @Query("password") String password, @Query("mode") String mode);

    @GET("/pat.pl")
    Call<GetPetCatModel> getPetCat(@Query("name") String name, @Query("password") String password, @Query("catid") String catId, @Query("lat") Double latitude, @Query("lng") Double longitude);

    @GET("/resetlist.pl")
    Call<GetResetListModel> getResetList(@Query("name") String name, @Query("password") String password);

    @GET("/changepass.pl")
    Call<GetChangePasswordModel> getChangePassword(@Query("name") String name, @Query("password") String password, @Query("newpass") String newPassword);

    @GET("/track.pl")
    Call<GetTrackCatModel> getTrackCat(@Query("name") String name, @Query("password") String password, @Query("catid") String catId, @Query("lat") Double latitude, @Query("lng") Double longitude);
}