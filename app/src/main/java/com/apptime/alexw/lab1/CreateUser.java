package com.apptime.alexw.lab1;

import android.Manifest;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import com.google.gson.*;
import retrofit2.*;



/**
 * Created by Alex W on 02/10/2017.
 * Camera and crop code adapted from CS65 sample project:
 * https://github.com/mishravarun/CS65-Samples/tree/master/Camera
 */
public class CreateUser extends AppCompatActivity {

    private Button mHaveAccountButton;

    private ImageView mImageView;

    private EditText mHandleEditText;
    private EditText mNameEditText;
    private EditText mPasswordEditText;

    private TextView mAvailableTextView;

    private String mName;
    private String mHandle;
    public String mPassword;

    private EditText mPasswordCheckEditText;
    private TextView mMatchesTextView;

    private AlertDialog mDialog;

    private View mView;

    private Boolean mNameEmpty;
    private Boolean mHandleEmpty;
    private Boolean mPasswordEmpty;

    private Boolean mPasswordMatch = true;

    public static final int REQUEST_CODE_TAKE_FROM_CAMERA = 0;

    private static final String URI_INSTANCE_STATE_KEY = "saved_uri";

    private Uri mImageCaptureUri;

    private boolean mIsTakenFromCamera;

    private LoginService mLoginService;

    // used if the user cancels the password matching dialog box to reset the passwordEdit text
    public void resetPassword(View view) {
        mDialog.hide();
        mPasswordEditText.setText("");
        mPasswordEditText.requestFocus();
    }


    //Checks if all fields are empty to chance clear button text (and later function when I have an account is active)
    private void notEmpty() {
        if (!(mNameEmpty && mPasswordEmpty && mHandleEmpty)) {
            mHaveAccountButton.setText("Clear");

        } else {
            mHaveAccountButton.setText("I already have an account");
        }
    }


    //Function of clear button, clears all editTexts
    public void clear(View view) {
        if (mHaveAccountButton.getText().toString().equals("Clear")) {
            mHandleEditText.setText("");
            mNameEditText.setText("");
            mPasswordEditText.setText("");
            mImageView.setImageResource(R.drawable.default_profile);
        } else {
            Intent intent = new Intent(this, Signin.class);
            startActivity(intent);
            finish();
        }
    }


    //Ensures all camera permissions are allowed
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT < 23)
            return;

        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 0);
        }
    }

    //
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED || grantResults[1] == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) || shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show an explanation to the user *asynchronously*
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("This permission is important for the app.")
                            .setTitle("Important permission required");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 0);
                            }

                        }
                    });
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 0);
                } else {
                    //Never ask again and handle your app without permission.
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the image capture uri before the activity goes into background
        outState.putParcelable(URI_INSTANCE_STATE_KEY, mImageCaptureUri);
    }

    public void onSaveClicked(View v) {
        // Save picture
        saveSnap();
        // Close the activity
        finish();
    }

    public void onChangePhotoClicked(View v) {
        // changing the profile image, show the dialog asking the user
        // to choose between taking a picture
        // Go to DialogFragmentHandler for details.
        displayDialog(DialogFragmentHandler.DIALOG_ID_PHOTO_PICKER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case REQUEST_CODE_TAKE_FROM_CAMERA:
                // Send image taken from camera for cropping
                beginCrop(mImageCaptureUri);
                break;

            case Crop.REQUEST_CROP: //We changed the RequestCode to the one being used by the library.
                // Update image view after image crop
                handleCrop(resultCode, data);

                // Delete temporary image taken by camera after crop.
                if (mIsTakenFromCamera) {
                    File f = new File(mImageCaptureUri.getPath());
                    if (f.exists())
                        f.delete();
                }

                break;
        }
    }

    //displays photo picker fragment
    public void displayDialog(int id) {
        DialogFragment fragment = DialogFragmentHandler.newInstance(id);
        fragment.show(getFragmentManager(),
                "Photo picker");
    }

    public void onPhotoPickerItemSelected(int item) {
        Intent intent;

        switch (item) {

            case DialogFragmentHandler.ID_PHOTO_PICKER_FROM_CAMERA:
                // Take photo from camera，
                // Construct an intent with action
                // MediaStore.ACTION_IMAGE_CAPTURE
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Construct temporary image path and name to save the taken
                // photo
                ContentValues values = new ContentValues(1);
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                mImageCaptureUri = getContentResolver()
                        .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);


                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        mImageCaptureUri);
                intent.putExtra("return-data", true);
                try {
                    // Start a camera capturing activity
                    // REQUEST_CODE_TAKE_FROM_CAMERA is an integer tag you
                    // defined to identify the activity in onActivityResult()
                    // when it returns
                    startActivityForResult(intent, REQUEST_CODE_TAKE_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                mIsTakenFromCamera = true;
                break;

            default:
                return;
        }

    }

    private void loadSnap() {

        // Load profile photo from internal storage
        try {
            FileInputStream fis = openFileInput("profile_photo.png");
            Bitmap bmap = BitmapFactory.decodeStream(fis);
            mImageView.setImageBitmap(bmap);
            fis.close();
        } catch (IOException e) {
            // Default profile photo if no photo saved before.
            mImageView.setImageResource(R.drawable.default_profile);
        }
    }

    private void saveSnap() {

        // Commit all the changes into preference file
        // Save profile image into internal storage.
        mImageView.buildDrawingCache();
        Bitmap bmap = mImageView.getDrawingCache();
        try {
            FileOutputStream fos = openFileOutput(
                    "profile_photo.png", MODE_PRIVATE);
            bmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Method to start Crop activity using the library
     * Earlier the code used to start a new intent to crop the image,
     * but here the library is handling the creation of an Intent, so you don't
     * have to.
     **/
    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            mImageView.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        //lots of initialization of UI elements
        mHaveAccountButton = findViewById(R.id.haveAccountButton);
        Button saveButton = findViewById(R.id.saveButton);

        mHandleEditText = findViewById(R.id.handleEditText);
        mNameEditText = findViewById(R.id.nameEditText);
        mPasswordEditText = findViewById(R.id.passwordEditText);

        mImageView = findViewById(R.id.imageView);

        mAvailableTextView = findViewById(R.id.availableTextView);

        mPasswordEmpty = (mPasswordEditText.getText().length() == 0);
        mHandleEmpty = (mHandleEditText.getText().length() == 0);
        mNameEmpty = (mNameEditText.getText().length() == 0);

        //Checks to see if any fields have data in, used to decide text of clear button
        notEmpty();

        //checks that all camera permissions are granted
        checkPermissions();

        //retrieves any profile picture that has been previously saved
        if (savedInstanceState != null) {
            mImageCaptureUri = savedInstanceState
                    .getParcelable(URI_INSTANCE_STATE_KEY);
        }

        //loads previous profile picture
        loadSnap();

        //Adds text change listener to passwordEditText
        mPasswordEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                //if text changes, password is no longer matched and notEmpty checks to see if the filed is empty to change clear button text
                mPasswordMatch = false;
                mPasswordEmpty = (mPasswordEditText.getText().length() == 0);
                notEmpty();
                mPassword = mPasswordEditText.getText().toString();


            }
        });

        //adds text change listener to handleEditText.
        mHandleEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                //checks if field empty
                mHandle = mHandleEditText.getText().toString();
                mHandleEmpty = (mHandleEditText.getText().length() == 0);
                notEmpty();

                //All usernames are currently available
                mAvailableTextView.setTextColor(Color.WHITE);

            }
        });

        //adds text change listener to nameEditText
        mNameEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                mName = mNameEditText.getText().toString();
                mNameEmpty = (mNameEditText.getText().length() == 0);
                notEmpty();


            }
        });

        //adds focus listener to passwordEditText to prompt password matching dialog when focus changes
        mPasswordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!mPassword.isEmpty() && !mPasswordMatch) {
                        popupMatchDialog();
                    }
                }
            }
        });

        //adds focus listener to handleEditText to check for availability when done with handle
        mHandleEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mHandle = mHandleEditText.getText().toString();
                if (!hasFocus) {
                    if (!mHandle.isEmpty()) {
                        mLoginService = LoginUtility.getLoginService();
                        sendGetNameAvailability(mHandle);
                    }
                }
            }
        });

        //if password does not match onCreate(e.g. if there is an orientation change while matching passwords), password matching dialog box should show
        if (!mPasswordMatch) popupMatchDialog();

        //retrieve saved user data
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mHandleEditText.setText(preferences.getString("Handle", null));
        mNameEditText.setText(preferences.getString("Name", null));
        mPasswordEditText.setText(preferences.getString("Password", null));
        mPasswordMatch = preferences.getBoolean("Match", false);

    }

    //function to show dialog box when password match is required
    private void popupMatchDialog() {

        if (!mPasswordMatch) {
            //creates dialog box
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(CreateUser.this).setCancelable(false);
            mView = getLayoutInflater().inflate(R.layout.password_match, null);
            mView.setBackgroundColor(Color.LTGRAY);

            mMatchesTextView = mView.findViewById(R.id.matchesTextView);
            mPasswordCheckEditText = mView.findViewById(R.id.passwordCheckEditText);

            if (mPasswordCheckEditText != null) {
                mPasswordCheckEditText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {

                        String attemptPassword = s.toString();

                        //if password is too long (i.e. definitley wrong) then it flashes red and resets
                        if (attemptPassword.length() > mPassword.length()) {
                            mPasswordCheckEditText.setTextColor(Color.RED);
                            //handler is used to free up UI thread, using system sleep resulted in a race condition that prevented the text flashing red
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    mPasswordCheckEditText.setText("");
                                    mPasswordCheckEditText.setTextColor(Color.WHITE);

                                }

                            }, 300);
                        }

                        //if password is correct, text flashes green and the dialog disappears
                        if (attemptPassword.equals(mPassword)) {

                            Toast.makeText(mView.getContext(), "Password matched correctly", Toast.LENGTH_SHORT).show();
                            mMatchesTextView.setText("Match Successful");
                            mPasswordCheckEditText.setTextColor(Color.GREEN);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {

                                @Override
                                public void run() {

                                    mDialog.hide();
                                    mPasswordMatch = true;
                                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                    SharedPreferences.Editor editor = preferences.edit();
                                    ;
                                    editor.putBoolean("Match", true);
                                    editor.commit();

                                }

                            }, 500);
                        }
                    }
                });
            }
            mBuilder.setView(mView);
            mDialog = mBuilder.create();
            mDialog.show();
        }
    }

    //functionality of save button
    public void save(View view) {

        //checks to see if all data has been entered correctly
        if (mHandleEmpty) Toast.makeText(this, "Please add a handle!", Toast.LENGTH_SHORT).show();

        else if (mNameEmpty) Toast.makeText(this, "Please add a name!", Toast.LENGTH_SHORT).show();

        else if (mPasswordEmpty)
            Toast.makeText(this, "Please add a password!", Toast.LENGTH_SHORT).show();

        else if (!mPasswordMatch) {
            Toast.makeText(this, "Whoops, make sure you match your password!", Toast.LENGTH_SHORT).show();
            popupMatchDialog();
        } else {
            // instantiate User object for JSON conversion and fire API POST call
            mLoginService = LoginUtility.getLoginService();

            UserProfileData user = new UserProfileData();
            user.setHandle(mHandle);
            user.setPassword(mPassword);
            user.setFullName(mName);

            sendPost(user);

            saveSnap();

            //if all data is entered correctly, data is saved to the device
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();

            // Edit the saved preferences
            editor.putString("Handle", mHandle);
            editor.putString("Name", mName);
            editor.putString("Password", mPassword);
            editor.putBoolean("Match", true);
            editor.putInt("Score", 0);
            editor.putFloat("Distance", 3);
            editor.putInt("MinTime", 1000);
            editor.commit();

            Toast.makeText(this, "Uploading Data", Toast.LENGTH_SHORT).show();
        }
    }

    // Sends a POST request to cs65 server with handle, password, and full name fields in JSON body
    // callback is handled asyncrynously and checks for server error as well as pre-existing accounts before pushing to the next activity
    public void sendPost(UserProfileData body) {
        mLoginService.postSaveProfile(body).enqueue(new Callback<PostSaveProfileModel>() {

            @Override
            public void onResponse(Call<PostSaveProfileModel> call, Response<PostSaveProfileModel> response) {

                if (response.isSuccessful()) {
                    Log.d("POST", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    PostSaveProfileModel model = gson.fromJson(json, PostSaveProfileModel.class);
                    String status = model.getStatus();
                    String data = model.getData();

                    Log.d("status", "status: " + status);
                    Log.d("data", "data: " + data);

                    if (status.equals("OK") && mAvailableTextView.getCurrentTextColor() == Color.GREEN) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "User Already Exists", Toast.LENGTH_SHORT).show();
                    }
                } else
                    Log.d("BAD", "post failed");
            }

            @Override
            public void onFailure(Call<PostSaveProfileModel> call, Throwable t) {
                Log.e("POST", "Server Connection Issue - Unable to submit post.");
                networkErrorMessage();
            }
        });
    }

    // pops a Toast telling user about the network error
    public void networkErrorMessage() {
        Toast.makeText(this, "Network Error - Cannot Create Account", Toast.LENGTH_SHORT).show();
    }

    // Sends a GET request to determine if the handle input is unique and available on the server or not
    // handles server error and sets availability icon to green or white where appropriate
    public void sendGetNameAvailability(String handle) {
        mLoginService.getNameAvailability(handle).enqueue(new Callback<GetNameAvailabilityModel>() {

            public void onResponse(Call<GetNameAvailabilityModel> call, Response<GetNameAvailabilityModel> response) {
                if (response.isSuccessful()) {
                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetNameAvailabilityModel model = gson.fromJson(json, GetNameAvailabilityModel.class);

                    String isAvailable = model.getAvail();
                    if (isAvailable.equals("true"))
                        mAvailableTextView.setTextColor(Color.GREEN);
                    else
                        mAvailableTextView.setTextColor(Color.WHITE);

                    Log.d("status", "avail: " + isAvailable);
                } else
                    Log.d("BAD", "post failed");
            }

            @Override
            public void onFailure(Call<GetNameAvailabilityModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getAvailability request.");
            }
        });
    }

}
