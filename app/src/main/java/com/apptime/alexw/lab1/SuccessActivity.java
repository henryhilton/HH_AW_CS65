package com.apptime.alexw.lab1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SuccessActivity extends AppCompatActivity {

    Button againButton;
    Button doneButton;
    TextView scoreTextView;
    LoginService mLoginService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        againButton = findViewById(R.id.againButton);
        doneButton = findViewById(R.id.doneButton);
        scoreTextView = findViewById(R.id.scoreTextView);

        mLoginService = LoginUtility.getLoginService();
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        int score = preferences.getInt("Score", 0);

        scoreTextView.setText("Look at you go, score: " + String.valueOf(score));
    }

    public void again(View view){
        //launches map activity to start the game
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        finish();
    }

    public void done(View view){
        //launches map activity to start the game
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}