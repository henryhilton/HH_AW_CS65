package com.apptime.alexw.lab1;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.varunmishra.catcameraoverlay.CameraViewActivity;
import com.varunmishra.catcameraoverlay.Config;
import com.varunmishra.catcameraoverlay.OnCatPetListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An activity that displays a map showing the place at the device's current location.
 */

public class MapsActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, OnCatPetListener  {

    GoogleMap googleMap;
    LocationManager locationManager;
    String locationProvider;
    MarkerOptions youMarker;

    Location myLocation;

    Boolean started = false;

    TextView catNameTextView;
    Button petButton;
    Button trackButton;

    Marker locationMarker;
    Marker mSelectedMarker;

    ImageView mCatImageView;

    LoginService mLoginService = LoginUtility.getLoginService();

    List<CatModel> mCatList = new ArrayList<>();
    List<Marker> mMarkerList = new ArrayList<>();

    CatModel selectedCat;
    CatModel trackingCat = null;

    double maxCatDistance;
    int minTime;

    private boolean mLocationPermissionGranted;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public static final String TAG = "MapsActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //set the layout
        setContentView(R.layout.activity_maps);
        mCatImageView = findViewById(R.id.logoImageView);

        petButton = findViewById(R.id.petButton);
        trackButton = findViewById(R.id.trackButton);
        catNameTextView = findViewById(R.id.catNameTextView);

        getLocationPermission();
        mLoginService = LoginUtility.getLoginService();
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        maxCatDistance = (double) preferences.getFloat("Distance", 3);
        minTime = preferences.getInt("MinTime", 500);

        //initialize the map
        if (this.googleMap == null) {

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

        }
        //initialize the location manager
        this.initializeLocationManager();

        trackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (trackButton.getText().toString().equals("Track")){
                    trackButton.setText("Stop");
                    trackingCat = selectedCat;
                    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove("Tracking");
                    editor.putString("Tracking", trackingCat.getName());
                    editor.commit();
                    trackButton.setBackgroundColor(Color.RED);
                    Intent startIntent = new Intent();
                    startIntent.setClass(getApplicationContext(), ForegroundService.class);
                    startIntent.putExtra("catID", selectedCat.getCatId());
                    startIntent.putExtra("catName", selectedCat.getName());
                    //startIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                    startService(startIntent);



                }
                else{
                    Intent service = new Intent();
                    Log.d("SERVVY", "Reached OnCLick");
                    service.setClass(getApplicationContext(), ForegroundService.class);
                    stopService(service);
                    stopMyService();
                }
            }
        });

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

//        if (isMyServiceRunning(ForegroundService.class)){
//
//            String catName = preferences.getString("Tracking", "Default Name");
//            goToCat(catName);
//        }


    }

    //used to move to cat being tracked on activity startup
    public void goToCat(String catName){

        CatModel selectedCat = null;
        Marker selectedMarker = null;

        for (Marker marker: mMarkerList) {
            if (marker.getTitle().equals(catName)) {
                selectedMarker = marker;
            }
        }

        for (CatModel cat : mCatList) {
            if (cat.getName().equals(catName)) {

                selectedCat = cat;
                trackingCat = cat;

                break;

            }

        }

        selectedMarker.showInfoWindow();

        googleMap.animateCamera(CameraUpdateFactory.newLatLng(selectedMarker.getPosition()), 17, null);

        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(selectedMarker.getPosition().latitude, selectedMarker.getPosition().longitude));
        this.googleMap.moveCamera(center);

        CameraUpdate zoom = CameraUpdateFactory.zoomTo(17);
        this.googleMap.animateCamera(zoom);

        catNameTextView.setText(selectedCat.getName() + ": " + String.format("%.2f", getCatDistance(selectedCat)) + " miles");
        petButton.setVisibility(View.VISIBLE);
        trackButton.setVisibility(View.VISIBLE);
        Picasso.with(getApplicationContext()).load(selectedCat.getPicUrl()).into(mCatImageView);
        selectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker_clicked));

        for (Marker m : mMarkerList) {
            if (m != selectedMarker)
                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker));
        }
        this.selectedCat = selectedCat;
        petButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MapsActivity.this.selectedCat.getPetted().equals("false")) {
                    sendGetPetCat(MapsActivity.this.selectedCat.getCatId());
                } else
                    Toast.makeText(getApplication(), "You've already petted this cat!", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap map) {

        googleMap = map;

        googleMap.setOnMarkerClickListener(this);

        Log.d(TAG, "onMapReady: Did this get called");
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        map.setTrafficEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);




        if (locationManager != null) {

            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionGranted = true;
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }

            Criteria criteria = new Criteria();
            String bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true));
            locationManager.requestLocationUpdates(bestProvider,1000,0,this);

            myLocation = locationManager.getLastKnownLocation(locationProvider);
            //initialize the location
            if (myLocation != null && googleMap != null) {
                onLocationChangedFirst(myLocation);
            }
        }

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String handle = preferences.getString("Handle", "Default Name");
        String password = preferences.getString("Password", "Default Password");
        String mode = "easy";


        sendGetCatList(handle, password, mode);

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng latLng) {

                if (mSelectedMarker != null) {
                    mSelectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker));
                    mSelectedMarker = null;
                    selectedCat = null;
                    stopMyService();
                    catNameTextView.setText("Try touching a cat!");
                    petButton.setVisibility(View.GONE);
                    trackButton.setVisibility(View.GONE);

                    mCatImageView.setImageResource(R.drawable.click_icon);
                }
            }
        });
    }






    @Override
    protected void onResume() {

        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("update-message"));

        Log.i("called", "Activity --> onResume");
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (locationProvider!= null) this.locationManager.requestLocationUpdates(this.locationProvider, minTime, 0, this);


    }

    @Override
    protected void onPause() {

        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);

        Log.i("called", "Activity --> onPause");

        this.locationManager.removeUpdates(this);
    }


    //-------------------------------------------
    //	Summary: initialize location manager
    //-------------------------------------------
    private void initializeLocationManager() {

        //get the location manager
        this.locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);


        //define the location manager criteria
        Criteria criteria = new Criteria();

        this.locationProvider = locationManager.getBestProvider(criteria, false);

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        if (locationProvider != null) locationManager.requestLocationUpdates(locationProvider,1000,0,this);
        if (locationProvider != null) myLocation = locationManager.getLastKnownLocation(locationProvider);

        //initialize the location
        if(myLocation != null && googleMap != null) {
            onLocationChangedFirst(myLocation);
        }

    }



    //------------------------------------------
    //	Summary: Location Listener  methods
    //------------------------------------------
    @Override
    public void onLocationChanged(Location location) {

        Log.i("called", "onLocationChanged");

        Log.d(TAG, "onLocationChanged: isMyServideRunning " + isMyServiceRunning(ForegroundService.class));
        if (myLocation != null) {
            myLocation = location;
        }

        if (locationMarker!= null) locationMarker.remove();
        youMarker = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("You");
        youMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.self_marker));
        locationMarker = googleMap.addMarker(youMarker);


        if (selectedCat != null){
            catNameTextView.setText(selectedCat.getName() + ": " + String.format("%.2f", getCatDistance(selectedCat)) + " miles");

        }
        
    }

    public void onLocationChangedFirst(Location location) {

        Log.i("called", "onFirstLocationChanged");

        //when the location changes, update the map by zooming to the location
        myLocation = location;
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude()));
        this.googleMap.moveCamera(center);
        if (locationMarker != null) locationMarker.remove();
        youMarker = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("You");
        youMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.self_marker));
        locationMarker = googleMap.addMarker(youMarker);


        CameraUpdate zoom=CameraUpdateFactory.zoomTo(17);
        this.googleMap.animateCamera(zoom);
    }

    @Override
    public void onProviderDisabled(String arg0) {

        Log.i("called", "onProviderDisabled");
    }

    @Override
    public void onProviderEnabled(String arg0) {

        Log.i("called", "onProviderEnabled");
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

        Log.i("called", "onStatusChanged");
    }


    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    // Retrofit API call to load list of cats into a java object
    public void sendGetCatList(final String handle, final String password, String mode) {

        mLoginService.getCatList(handle, password, mode).enqueue(new Callback<List<CatModel>>() {

            public void onResponse(Call<List<CatModel>> call, Response<List<CatModel>> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json

                    Type founderListType = new TypeToken<List<CatModel>>(){}.getType();
                    mCatList = gson.fromJson(json, founderListType);

                    List<CatModel> catsToBeRemoved = new ArrayList<>();
                    for (CatModel cat:mCatList){
                        Log.d(TAG, "onResponse: " + getCatDistance(cat));
                        if (getCatDistance(cat) > maxCatDistance) catsToBeRemoved.add(cat);
                    }
                    Log.d(TAG, "onResponse: " + catsToBeRemoved.toString());
                    mCatList.removeAll(catsToBeRemoved); //used to fix java.util.ConcurrentModificationException or removing while using iterator


                    showCatMarkers();
                }
                else
                    Log.d("BAD", "post failed");
            }
            @Override
            public void onFailure(Call<List<CatModel>> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getCatList request.");
                Toast.makeText(getApplicationContext(), "Network Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Retrofit API call to try to pet a cat if within distance
    public void sendGetPetCat(final String catID) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String name = preferences.getString("Handle", "Default Name");
        String password = preferences.getString("Password", "Default Password");
        Double latitude = myLocation.getLatitude();
        Double longitude = myLocation.getLongitude();

        mLoginService.getPetCat(name, password, catID, latitude, longitude).enqueue(new Callback<GetPetCatModel>() {

            public void onResponse(Call<GetPetCatModel> call, Response<GetPetCatModel> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetPetCatModel model = gson.fromJson(json, GetPetCatModel.class);

                    Log.d("GET", "pet cat " + model.getCatId());
                    Log.d("Status", "status: " + model.getStatus());
                    Log.d("ERROR", "error: " + model.getCode() + " reason: " + model.getReason());



                    if (model.getStatus().equals("OK")){
                        for (CatModel cat: mCatList){
                            if (cat.getCatId().equals(catID)){
                                launchCameraCatSearch();
                                stopMyService();
                            }
                        }
                    }
                    else{
                        Toast.makeText(getApplication(), model.getReason(), Toast.LENGTH_LONG).show();
                    }

                }
                else
                    Log.d("BAD", "post failed. Cat Id:" + catID);

            }
            @Override
            public void onFailure(Call<GetPetCatModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getPetCat request.");
                Toast.makeText(getApplicationContext(), "Network Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

    // redraws cat markers and handles any active foreground service to maintain tracking fucntionality
     public void showCatMarkers(){
         for (CatModel cat: mCatList){

             MarkerOptions catMarker = new MarkerOptions().position(new LatLng(cat.getLat(), cat.getLng())).title(cat.getName());
             catMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker));
             Marker marker = googleMap.addMarker(catMarker);
             mMarkerList.add(marker);

         }

         if (isMyServiceRunning(ForegroundService.class)){
             final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

             String catName = preferences.getString("Tracking", "Default Name");
             goToCat(catName);
             started = true;
         }
         else if (!started) selectClosestCat();
     }


     public double getCatDistance(CatModel cat){
         Location catLocation = new Location("");//provider name is unnecessary
         catLocation.setLatitude(cat.getLat());
         catLocation.setLongitude(cat.getLng());

         if (myLocation != null)
         return myLocation.distanceTo(catLocation)*0.000621371192; //converts to miles
         else
             return 0;
     }

    @Override
    public boolean onMarkerClick(Marker marker) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String handle = preferences.getString("Handle", "Default Name");
        String password = preferences.getString("Password", "Default Password");
        String mode = "easy";

        sendGetCatList(handle, password, mode);

        if (marker.getTitle().equals("You")){
            if (mSelectedMarker != null) {
                mSelectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker));
                mSelectedMarker = null;
                selectedCat = null;
                stopMyService();
                catNameTextView.setText("Try touching a cat!");
                petButton.setVisibility(View.GONE);
                trackButton.setVisibility(View.GONE);
                mCatImageView.setImageResource(R.drawable.click_icon);
            }
        }
        else {
            if (null != mSelectedMarker) {
                mSelectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker));
            }
            mSelectedMarker = marker;
            final String catName = marker.getTitle();
            selectedCat = null;
            for (CatModel cat: mCatList){
                if (cat.getName().equals(catName)){
                    selectedCat = cat;
                    if (selectedCat != trackingCat) stopMyService();
                }
            }

            mSelectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker_clicked));
            if (selectedCat != null){
                catNameTextView.setText(selectedCat.getName() + ": " + String.format("%.2f", getCatDistance(selectedCat)) + " miles");
                petButton.setVisibility(View.VISIBLE);
                trackButton.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(selectedCat.getPicUrl()).into(mCatImageView);

                petButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (selectedCat.getPetted().equals("false")) {
                            sendGetPetCat(selectedCat.getCatId());
                        }
                        else Toast.makeText(getApplication(), "You've already petted this cat!", Toast.LENGTH_LONG).show();
                    }
                });


            }
            }
            return false;
        }

    // launches the Cat Camera Overlay Library and passes it necessary settings/data
    public void launchCameraCatSearch(){
        Config.catName = selectedCat.getName();
        Config.catLatitude = selectedCat.getLat();
        Config.catLongitude = selectedCat.getLng();
        Config.locDistanceRange = 30;
        Config.useLocationFilter = true;     // use this only for testing. This should be true in the final app.
        Config.onCatPetListener = this;

        Picasso.with(this)
                .load(selectedCat.getPicUrl())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from) {
                        Config.catImage = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                    }
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {}
                });

        Intent i = new Intent(this, CameraViewActivity.class);
        startActivity(i);
    }

    //extra credit -- selects the closest cat upon launching the map activity
    public void selectClosestCat(){

         started = true;
         CatModel closestCat = null;
         Marker closestMarker = null;
         Double smallestDistance = 1000000.0;

         for (Marker marker: mMarkerList){
             String catName = marker.getTitle();
             for (CatModel cat: mCatList){
                 if (cat.getName().equals(catName)){
                     double dist = getCatDistance(cat);
                     if (dist < smallestDistance){
                         smallestDistance = dist;
                         closestCat = cat;
                         closestMarker = marker;
                     }
                     break;
                 }

             }

             closestMarker.showInfoWindow();

             googleMap.animateCamera(CameraUpdateFactory.newLatLng(closestMarker.getPosition()), 17, null);

             CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(closestMarker.getPosition().latitude, closestMarker.getPosition().longitude));
             this.googleMap.moveCamera(center);

             CameraUpdate zoom=CameraUpdateFactory.zoomTo(17);
             this.googleMap.animateCamera(zoom);

             catNameTextView.setText(closestCat.getName() + ": " + String.format("%.2f", getCatDistance(closestCat)) + " miles");
             petButton.setVisibility(View.VISIBLE);
             trackButton.setVisibility(View.VISIBLE);
             Picasso.with(getApplicationContext()).load(closestCat.getPicUrl()).into(mCatImageView);
             closestMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker_clicked));

             for (Marker m:mMarkerList){
                 if (m != closestMarker) m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.cat_marker));
             }
             selectedCat = closestCat;
             petButton.setOnClickListener(new View.OnClickListener() {
                 public void onClick(View v) {
                     if (selectedCat.getPetted().equals("false")) {
                         sendGetPetCat(selectedCat.getCatId());
                     }
                     else Toast.makeText(getApplication(), "You've already petted this cat!", Toast.LENGTH_LONG).show();
                 }
             });
         }
    }

    // handles actions after a cat is pet using callback method from cat camera overlay library
    @Override
    public void onCatPet(String catName) {
        Toast.makeText(this,"You just Pet - " + catName, Toast.LENGTH_LONG).show();

        selectedCat.setPetted("true");

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        int score = preferences.getInt("Score", 0);
        score++;
        editor.putInt("Score", score);
        editor.commit();

        Intent intent = new Intent(getApplication(), SuccessActivity.class);
        startActivity(intent);
        finish();
    }


    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Get extra data included in the Intent: distance
            double distance = intent.getDoubleExtra("message", 0);
            Log.d("receiver", "Got message: " + distance);

        }
    };

    //check if service is running
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                trackButton.setText("Stop");
                trackingCat = selectedCat;
                trackButton.setBackgroundColor(Color.RED);
                return true;
            }
        }
        trackButton.setText("Track");
        trackingCat = null;
        trackButton.setBackgroundColor(Color.GREEN);
        Intent stopIntent = new Intent(getApplicationContext(), ForegroundService.class);
        stopIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(stopIntent);
        return false;
    }

    //resets track button and stops foreground service
    private void stopMyService(){
        if (isMyServiceRunning(ForegroundService.class)){
            trackButton.setText("Track");
            trackingCat = null;
            trackButton.setBackgroundColor(Color.GREEN);

            Intent stopIntent = new Intent(getApplicationContext(), ForegroundService.class);
            stopIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
            stopService(stopIntent);
        }
    }


}