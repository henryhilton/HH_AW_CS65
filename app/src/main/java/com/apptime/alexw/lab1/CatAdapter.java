package com.apptime.alexw.lab1;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex W on 13/11/2017.
 */

public class CatAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<CatModel> mDataSource;



    public CatAdapter(Context context, List<CatModel> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.d("CatAdapter", "CatAdapter: We got here");
    }

    //1
    @Override
    public int getCount() {
        return mDataSource.size();
    }

    //2
    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    //3
    @Override
    public long getItemId(int position) {
        return position;
    }

    //4
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get view for row item
        View rowView = mInflater.inflate(R.layout.history_list_item, parent, false);

        TextView nameTextView = rowView.findViewById(R.id.nameTextView);
        TextView latTextView = rowView.findViewById(R.id.latTextView);
        TextView longTextView = rowView.findViewById(R.id.longTextView);

        ImageView catImageView = rowView.findViewById(R.id.catImageView);
        ImageView heartImageView = rowView.findViewById(R.id.heartImageView);

        CatModel cat = (CatModel) getItem(position);

        nameTextView.setText(cat.getName());
        latTextView.setText("Lat: " + Double.toString(cat.getLat()));
        longTextView.setText("Lng: " + Double.toString(cat.getLng()));

        Picasso.with(mContext).load(cat.getPicUrl()).into(catImageView);

        if (cat.getPetted().toString().equals("true")){
            heartImageView.setImageResource(R.drawable.red_heart);
        }
        else heartImageView.setImageResource(R.drawable.grey_heart);



        return rowView;
    }



}
