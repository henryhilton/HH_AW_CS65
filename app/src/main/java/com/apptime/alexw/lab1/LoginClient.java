package com.apptime.alexw.lab1;

/**
 * Created by henryhilton on 10/10/17.
 */


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// Creates the retrofit instance and integrates Gson
public class LoginClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl) {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
