package com.apptime.alexw.lab1;


import android.content.Intent;

import android.content.SharedPreferences;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alex W on 10/10/2017.
 */

// instantiates the play fragment and handles a button
public class PlayFragment extends Fragment {

    private static final String TAG = "PlayFragment";
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private Button playButton;
    private TextView nameTextView;
    private TextView catNumTextView;

    LoginService mLoginService;

    List<CatModel> mCatList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_play,container,false);
        playButton = (Button) view.findViewById(R.id.btnTest);
        nameTextView = view.findViewById(R.id.nameTextView);
        catNumTextView = view.findViewById(R.id.catNumTextView);

        mLoginService = LoginUtility.getLoginService();

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String name = preferences.getString("Handle", "Default Name");
        String password = preferences.getString("Password", "Default Password");
        String mode = "easy";
        sendGetCatList(name, password, mode);

        nameTextView.setText("Welcome " + name);



        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    //launches map activity to start the game
                    Intent intent = new Intent(getActivity(), MapsActivity.class);
                    startActivity(intent);

                } else {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                }

            }
        });

        return view;
    }


    public void sendGetCatList(final String handle, final String password, String mode) {
        //Toast.makeText(getApplicationContext(), "Getting Cat List", Toast.LENGTH_SHORT).show();

        mLoginService.getCatList(handle, password, mode).enqueue(new Callback<List<CatModel>>() {

            public void onResponse(Call<List<CatModel>> call, Response<List<CatModel>> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json

                    Type founderListType = new TypeToken<List<CatModel>>() {
                    }.getType();
                    mCatList = gson.fromJson(json, founderListType);
                    int num = 0;
                    for (CatModel cat: mCatList){
                        if (cat.getPetted().equals("false")){
                            Log.d("Petted", cat.getPetted());
                            num++;
                        }
                    }

                    catNumTextView.setText("There are " + Integer.toString(num) + " cats to pet");
                }
                else
                    Log.d("BAD", "post failed");
            }
            @Override
            public void onFailure(Call<List<CatModel>> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getCatList request.");
                Toast.makeText(getContext(), "Network Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }



}