package com.apptime.alexw.lab1;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alex W on 10/10/2017.
 */

// instantiates History fragement in tab layout and loads a button
public class HistoryFragment extends Fragment{

    private static final String TAG = "HistoryFragment";
    LoginService mLoginService;
    List<CatModel> mCatList = new ArrayList<>();
    ListView mListView;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history,container,false);

        mListView = view.findViewById(R.id.listView);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String handle = preferences.getString("Handle", "Default Name");
        String password = preferences.getString("Password", "Default Password");
        String mode = "easy";

        mLoginService = LoginUtility.getLoginService();
        sendGetCatList(handle, password, mode);
        Log.d(TAG, "onCreateView: " + mCatList.toString());




        return view;
    }

    public void sendGetCatList(final String handle, final String password, String mode) {

        mLoginService.getCatList(handle, password, mode).enqueue(new Callback<List<CatModel>>() {

            public void onResponse(Call<List<CatModel>> call, Response<List<CatModel>> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json

                    Type founderListType = new TypeToken<List<CatModel>>() {
                    }.getType();
                    mCatList = gson.fromJson(json, founderListType);
                    CatAdapter adapter = new CatAdapter(getContext(), mCatList);
                    mListView.setAdapter(adapter);


                }
                else
                    Log.d("BAD", "post failed");
            }
            @Override
            public void onFailure(Call<List<CatModel>> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getCatList request.");
                Toast.makeText(getContext(), "Network Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
