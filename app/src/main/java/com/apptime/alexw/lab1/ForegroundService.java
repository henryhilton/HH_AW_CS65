package com.apptime.alexw.lab1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alex W on 14/11/2017.
 */

public class ForegroundService extends Service implements LocationListener{

    private static final String TAG = "ForegroundService";
    private static final int NOTIF_ID=1;


    //used for keep track on Android running status
    public static Boolean mIsServiceRunning = false;

    private Location myLocation;
    private String catID;
    LoginService mLoginService = LoginUtility.getLoginService();
    Notification notification;
    private double distance;
    private String catName;
    private Boolean notified = false;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: In onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Stops the service. Bug is not here, this calls successfully and starts onDestroy, but somehow the service comes to life again after that
        if (intent.getAction() != null && intent.getAction().equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
            Log.d(TAG, "Received Stop Foreground Intent");
            stopForeground(true);
            stopSelf();

        }
        else{
            //start the tracking
            catID = intent.getStringExtra("catID");
            catName = intent.getStringExtra("catName");

            Log.d(TAG, "onStartCommand: Received Start Foreground Intent");
            getLocation();
            sendGetTrackCat(catID);


            mIsServiceRunning = true; // set service running status = true
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //works to stop the service, but some phantom service starts up again after this closes
    @Override
    public void onDestroy() {
        super.onDestroy();
        //Notification notification = getMyActivityNotification(text);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
        stopForeground(true);

        stopSelf();


        mIsServiceRunning = false;
        Log.d(TAG, "In onDestroy");
    }

    //tracks the cat
    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: called");
        myLocation = location;
        sendGetTrackCat(catID);
        sendNotification();

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    //tracking call being sent from here
    public void sendGetTrackCat(final String catID) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String name = preferences.getString("Handle", "Default Name");
        String password = preferences.getString("Password", "Default Password");
        Double latitude = myLocation.getLatitude();
        Double longitude = myLocation.getLongitude();

        mLoginService.getTrackCat(name, password, catID, latitude, longitude).enqueue(new Callback<GetTrackCatModel>() {

            public void onResponse(Call<GetTrackCatModel> call, Response<GetTrackCatModel> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetTrackCatModel model = gson.fromJson(json, GetTrackCatModel.class);

                    // returns status of OK or ERROR. OK comes with a distance and bearing to the cat, ERROR comes with a code and reason for the error.

                    Log.d("GET", "track cat:" + model.getCatId() + "distance:" + model.getDistance() + "bearing: " + model.getBearing());
                    Log.d("Status", "status: " + model.getStatus());
                    Log.d("ERROR", "error: " + model.getCode() + " reason: " + model.getReason());
                    if (model!=null && model.getStatus() !=null) {
                        if (model.getStatus().equals("OK")) {
                            // do whatever tracking stuff you need with model.getDistance() / model.getBearing()
                            distance = model.getDistance();
                            updateNotification();
                        } else {
                            Toast.makeText(getApplicationContext(), model.getReason(), Toast.LENGTH_LONG).show();
                            // handle any other error cases
                        }
                    }
                }
                else
                    Log.d("BAD", "post failed. Cat Id:" + catID);

            }
            @Override
            public void onFailure(Call<GetTrackCatModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getPetCat request.");
                Toast.makeText(getApplicationContext(), "Network Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

    //creates the updated notification
    private Notification getMyActivityNotification(String text){
        // The PendingIntent to launch our activity if the user selects
        // this notification
        CharSequence title = "Now tracking " + catName;

        Intent notificationIntent = new Intent(this, MapsActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        Intent stopIntent = new Intent(this, ForegroundService.class);
        stopIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        PendingIntent sstopIntent = PendingIntent.getService(this, 0,
                stopIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);

        Notification.Builder builder = new Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.drawable.cat_marker)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true);
        builder.addAction(0, "Stop", sstopIntent);
        notification = builder.build();
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);
        return notification;
    }

    /**
     * This is the method that can be called to update the Notification
     */
    private void updateNotification() {
        String text = distance + " meters away";

        Notification notification = getMyActivityNotification(text);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIF_ID, notification);
    }

    //gets location on first start of the service
    protected void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            Criteria criteria = Constants.getCriteria();
            String provider;
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (locationManager != null) {
                provider = locationManager.getBestProvider(criteria, true);
                myLocation = locationManager.getLastKnownLocation(provider);

                //this line is necessary to make sure you continue to update location after the first request
                locationManager.requestLocationUpdates(provider,0,0,this);
            }
        }
    }

    //checks whether to send notification that user is close to a cat while tracking
    protected void sendNotification(){
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        boolean sound = preferences.getBoolean("Sound", true);
        int notiDist = preferences.getInt("NotiDist", 20);
        if (distance < notiDist && !notified) {

            NotificationManager notificationManager = (NotificationManager) getSystemService(ForegroundService.NOTIFICATION_SERVICE);

            Intent notificationIntent = new Intent(this, MapsActivity.class);
            notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);


            Notification.Builder builder1 = new Notification.Builder(this)
                    .setContentTitle("You're close to " + catName)
                    .setSmallIcon(R.drawable.cat_marker)
                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(pendingIntent)
                    .setOngoing(false);
            Notification noti = builder1.build();

            //if the settings dictate it, send a vibration and a sound with the notification
            noti.defaults |= Notification.DEFAULT_VIBRATE;
            if (sound) noti.defaults |= Notification.DEFAULT_SOUND;
            notificationManager.notify(Constants.ACTION.SERVICE_PUSH_NOTIFICATION_CHANNEL, noti);
            notified = true;
        }

        else if (distance > notiDist && notified) notified = false;
    }
}
