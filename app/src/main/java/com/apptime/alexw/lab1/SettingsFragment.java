package com.apptime.alexw.lab1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.nearby.messages.Distance;
import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alex W on 10/10/2017.
 */

public class SettingsFragment extends Fragment {
    private static final String TAG = "SettingsFragment";

    private LoginService mLoginService;

    private Button mLogoutButton;
    private ImageView mProfileImageView;
    private TextView mNameTextView;
    private TextView scoreTextView;
    private Button resetButton;
    private Button resetCatsButton;
    private SeekBar distanceSeekBar;
    private Switch soundSwitch;

    private TextView distanceTextView;
    private TextView timeTextView;
    private EditText timeEditText;
    private EditText notiDistEditText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings,container,false);
        mLogoutButton = view.findViewById(R.id.logoutButton);
        mProfileImageView = view.findViewById(R.id.imageView);
        mNameTextView = view.findViewById(R.id.nameTextView);
        scoreTextView = view.findViewById(R.id.scoreTextView);
        resetButton = view.findViewById(R.id.resetButton);
        resetCatsButton = view.findViewById(R.id.resetCatsButton);
        distanceSeekBar = view.findViewById(R.id.seekBar);
        distanceTextView = view.findViewById(R.id.distanceTextView);
        timeTextView = view.findViewById(R.id.timeTextView);
        timeEditText = view.findViewById(R.id.timeEditText);
        soundSwitch = view.findViewById(R.id.soundSwitch);
        notiDistEditText = view.findViewById(R.id.notiDistEditText);


        // fires off the get profile API call using handle and password data from shared preferences
        mLoginService = LoginUtility.getLoginService();
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String handle =  preferences.getString("Handle", "Default User");
        String password = preferences.getString("Password", "Default Password");
        int score = preferences.getInt("Score", 0);
        float distance = preferences.getFloat("Distance", 3);
        final int minTime = preferences.getInt("MinTime", 1000);
        boolean sound = preferences.getBoolean("Sound", true);
        int notiDist = preferences.getInt("NotiDist", 20);

        soundSwitch.setChecked(sound);

        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("Sound", b);
                editor.commit();
            }
        });

        distanceSeekBar.setMax(30);
        distanceSeekBar.setProgress((int)distance * 10);
        distanceTextView.setText("Max Cat Dist: " + String.valueOf((double)distanceSeekBar.getProgress() / 10) + " miles");

        timeEditText.setText(String.valueOf(minTime));

        timeEditText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {


                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = preferences.edit();
                if (timeEditText.getText().toString().length() != 0) {
                    editor.putInt("MinTime", Integer.valueOf(timeEditText.getText().toString()));
                    editor.commit();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        notiDistEditText.setText(String.valueOf(notiDist));

        notiDistEditText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {


                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = preferences.edit();
                if (notiDistEditText.getText().toString().length() != 0) {
                    editor.putInt("NotiDist", Integer.valueOf(notiDistEditText.getText().toString()));
                    editor.commit();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });



        distanceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                distanceTextView.setText("Max Cat Dist: " + String.valueOf((double)distanceSeekBar.getProgress() / 10) + " miles");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = preferences.edit();
                Float distance = (float) ((double)distanceSeekBar.getProgress() / 10);
                editor.putFloat("Distance", distance);
                editor.commit();
            }
        });


        scoreTextView.setText("Your score: " + Integer.toString(score));
        sendGetProfile(handle,password);

        resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("Score", 0);
                editor.commit();

                scoreTextView.setText("Your score: 0");
                Toast.makeText(getContext(), "Score reset, go pet some cats!", Toast.LENGTH_SHORT).show();

            }
        });

        resetCatsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String handle =  preferences.getString("Handle", "Default User");
                String password = preferences.getString("Password", "Default Password");
                sendGetResetList(handle, password);
                Toast.makeText(getContext(), "Cat list reset, time to pet some new cats!", Toast.LENGTH_SHORT).show();

            }
        });


        // handles sign out, clearing saved data and sending user back to sign in page
        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Signed Out",Toast.LENGTH_SHORT).show();
                final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(getActivity(), Signin.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return view;
    }

    private void loadSnap() {

        // Load profile photo from internal storage
        try {
            FileInputStream fis = getActivity().openFileInput("profile_photo.png");
            Bitmap bmap = BitmapFactory.decodeStream(fis);
            mProfileImageView.setImageBitmap(bmap);
            fis.close();
        } catch (IOException e) {
            // Default profile photo if no photo saved before.
            mProfileImageView.setImageResource(R.drawable.default_profile);
        }
    }

    // Sends GET request with username and password auuthentication details to download profile data from server
    // Implementation here is unnecessary but an example of functionality -- used to display the handle in settings screen
    public void sendGetProfile(String handle, String password) {
        mLoginService.getProfile(handle, password).enqueue(new Callback<GetProfileModel>() {

            public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetProfileModel model = gson.fromJson(json, GetProfileModel.class);
                    String username = model.getName();
                    String error = model.getError();

                    Log.d("GET", "handle: " + username);
                    Log.d("ERROR", "error: " + error);

                    mNameTextView.setText("Welcome: " + username);
                }
                else
                    Log.d("BAD", "post failed");
            }
            @Override
            public void onFailure(Call<GetProfileModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getProfile request.");
            }
        });
    }

    public void sendGetResetList(String name, String password) {

        mLoginService.getResetList(name, password).enqueue(new Callback<GetResetListModel>() {

            public void onResponse(Call<GetResetListModel> call, Response<GetResetListModel> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetResetListModel model = gson.fromJson(json, GetResetListModel.class);

                    Log.d("Status", "status: " + model.getStatus());
                    Log.d("ERROR", "error: " + model.getCode() + " reason: " + model.getError());


                }
                else
                    Log.d("BAD", "post failed");
            }
            @Override
            public void onFailure(Call<GetResetListModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getResetList request.");
                Toast.makeText(getContext(), "Network Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }




}