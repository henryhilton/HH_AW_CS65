package com.apptime.alexw.lab1;

import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alex W on 10/10/2017.
 */

// THIS CLASS IS NOT IN USE

// instantiates ranking fragment and handles a button
public class RankingFragment extends Fragment{
    private static final String TAG = "RankingFragment";

    private Button btnTEST;

    private Location myLocation;

    LoginService mLoginService = LoginUtility.getLoginService();

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_ranking,container,false);
//        btnTEST = (Button) view.findViewById(R.id.btnTest);
//
//        btnTEST.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Clicked the ranking button",Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        return view;
//    }

    public void sendGetTrackCat(final String catID) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String name = preferences.getString("Handle", "Default Name");
        String password = preferences.getString("Password", "Default Password");
        Double latitude = myLocation.getLatitude();
        Double longitude = myLocation.getLongitude();

        mLoginService.getTrackCat(name, password, catID, latitude, longitude).enqueue(new Callback<GetTrackCatModel>() {

            public void onResponse(Call<GetTrackCatModel> call, Response<GetTrackCatModel> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response.body()));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetTrackCatModel model = gson.fromJson(json, GetTrackCatModel.class);

                    // returns status of OK or ERROR. OK comes with a distance and bearing to the cat, ERROR comes with a code and reason for the error.

                    Log.d("GET", "track cat:" + model.getCatId() + "distance:" + model.getDistance() + "bearing: " + model.getBearing());
                    Log.d("Status", "status: " + model.getStatus());
                    Log.d("ERROR", "error: " + model.getCode() + " reason: " + model.getReason());

                    if (model.getStatus().equals("OK")){
                            // do whatever tracking stuff you need with model.getDistance() / model.getBearing()
                    }
                    else{
                        Toast.makeText(getContext(), model.getReason(), Toast.LENGTH_LONG).show();
                        // handle any other error cases
                    }

                }
                else
                    Log.d("BAD", "post failed. Cat Id:" + catID);

            }
            @Override
            public void onFailure(Call<GetTrackCatModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getPetCat request.");
                Toast.makeText(getContext(), "Network Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

}