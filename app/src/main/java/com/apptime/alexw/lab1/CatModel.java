package com.apptime.alexw.lab1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by henryhilton on 10/30/17.
 */

public class CatModel {

//            "catId":  "1",
//            "name":   "Fluffy",
//            "picUrl": "http://cs65.cs.dartmouth.edu/kitty/<..>",
//            "lat": 79.172653871,
//            "lng": 76.127368,
//            "petted": "false"

        @SerializedName("catId")
        @Expose
        private String catId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("picUrl")
        @Expose
        private String picUrl;
        @SerializedName("lat")
        @Expose
        private Double lat;
        @SerializedName("lng")
        @Expose
        private Double lng;
        @SerializedName("petted")
        @Expose
        private String petted;
        @SerializedName("error")
        @Expose
        private String error;
        @SerializedName("code")
        @Expose
        private String code;


        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        public Double getLng() {
            return lng;
        }

        public void setLng(Double lng) {
            this.lng = lng;
        }

        public String getPetted() {
            return petted;
        }

        public void setPetted(String petted) {
            this.petted = petted;
        }

        public String getCode() {
        return code;
    }

        public void setCode(String code) {
        this.code = code;
    }

        public String getError() {
        return error;
    }

        public void setError(String error) {
        this.error = error;
    }

 }