package com.apptime.alexw.lab1;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alex W on 10/10/2017.
 */

public class Signin extends Activity {

    EditText mHandleEditText;
    EditText mPasswordEditText;
    Button mLoginButton;
    private LoginService mLoginService = LoginUtility.getLoginService();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        ImageView imageView = findViewById(R.id.logoImageView);

        mLoginButton = findViewById(R.id.signinButton);
        mHandleEditText = findViewById(R.id.handleEditText);
        mPasswordEditText = findViewById(R.id.passwordEditText);

        imageView.setImageResource(R.drawable.logo);

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String autoHandle =  preferences.getString("Handle", null);
        String autoPassword = preferences.getString("Password", null);

        if (autoHandle != null && autoPassword != null){
            sendAutoLogin(autoHandle,autoPassword);
        }
    }

    // clears any saved data from shared prefs and send user to create user activity
    public void createAccount(View v){
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        Intent intent = new Intent(this, CreateUser.class);
        startActivity(intent);
        finish();
    }

    public void clear(View v){
        mPasswordEditText.setText("");
        mHandleEditText.setText("");
    }

    // fires a get call to check sign in with inputted details
    public void login(View v){
        if (mHandleEditText.getText().toString().length()!=0 && mPasswordEditText.getText().toString().length()!=0) {
            Log.d("handle", mHandleEditText.getText().toString());
            Log.d("password", mPasswordEditText.getText().toString());
            sendGetProfile(mHandleEditText.getText().toString(), mPasswordEditText.getText().toString());
        }
        else Toast.makeText(this, "Make sure you add something to both fields!", Toast.LENGTH_SHORT).show();
    }

    // Sends GET request with username and password auuthentication details to download profile data from server
    // Implementation here is unnecessary but an example of functionality -- used to display the handle in settings screen
    public void sendGetProfile(String handle, String password) {

        mLoginService.getProfile(handle, password).enqueue(new Callback<GetProfileModel>() {

            public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetProfileModel model = gson.fromJson(json, GetProfileModel.class);
                    String username = model.getName();
                    String error = model.getError();
                    String password = model.getPassword();

                    Log.d("GET", "handle: " + username);
                    Log.d("ERROR", "error: " + error);

                    if(error == null) {
                        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("Handle", username);
                        editor.putString("Password", password);
                        editor.commit();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                    Log.d("BAD", "post failed, response code: " + response.code());
                    Toast.makeText(getApplication(), "Server Connection Error",Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<GetProfileModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getProfile request.");
                Toast.makeText(getApplication(), "Server Connection Error",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void sendAutoLogin(String handle, String password) {

        mLoginService.getProfile(handle, password).enqueue(new Callback<GetProfileModel>() {

            public void onResponse(Call<GetProfileModel> call, Response<GetProfileModel> response) {

                if(response.isSuccessful()) {

                    Log.d("GET", "body: " + new Gson().toJson(response));

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body()); // serializes target to Json
                    GetProfileModel model = gson.fromJson(json, GetProfileModel.class);
                    String username = model.getName();
                    String error = model.getError();
                    String password = model.getPassword();

                    Log.d("GET", "handle: " + username);
                    Log.d("ERROR", "error: " + error);

                    if(error == null) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
                else
                    Log.d("BAD", "post failed, response code: " + response.code());

            }
            @Override
            public void onFailure(Call<GetProfileModel> call, Throwable t) {
                Log.e("GET", "Server Connection Issue -- Unable to submit getProfile request.");
            }
        });
    }

}