package com.apptime.alexw.lab1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by henryhilton on 11/6/17.
 */

public class GetTrackCatModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("bearing")
    @Expose
    private Double bearing;
    @SerializedName("catId")
    @Expose
    private String catId;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("reason")
    @Expose
    private String reason;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getBearing() {
        return bearing;
    }

    public void setBearing(Double bearing) {
        this.bearing = bearing;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}