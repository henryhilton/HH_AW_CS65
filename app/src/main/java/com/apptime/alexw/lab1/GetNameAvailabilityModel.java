package com.apptime.alexw.lab1;

/**
 * Created by henryhilton on 10/10/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// Data Model for name availability JSON response serialization
public class GetNameAvailabilityModel {

    @SerializedName("avail")
    @Expose
    private String avail;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("error")
    @Expose
    private String error;

    public String getAvail() {
        return avail;
    }

    public void setAvail(String avail) {
        this.avail = avail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
